These things should be tested on mac:
  adubapi - adubapi.s_adbrbPath variable should be set to adb.rb file path. Check if that works.
  adubapi.ExecD - does executing command actually prints its output to console. Solution -> add print() method
  adubapi.ExecDc - check how does copy pasta works on both OSes
  adubapi.ExecIns - fully check this function.
    * Does it install application?
    * Test global variables
    * Stress test it
  adubapi.record - popen is pipe and not a process, so it will be difficult (is it even possible though?) to control it. Workaround this somehow or find a way to use processes. Dont forget that unix with ctrl+c terminates whole process without asking any question.
  adubapi.screen - test this method. Python shouldn't call another python script via new process, it can and should be done directly
  adubapi.seen - test this method
