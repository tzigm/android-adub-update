from multiprocessing import Process
import subprocess
import time
import signal
import sys
import os

class Recorder(object):
    pulldir = ""
    vidname = "tempvid.mp4"

    cmd = "adb shell screenrecord /sdcard/%s" % vidname
    cmd2 = "adb pull /sdcard/%s %s" % (vidname, pulldir)
    cmd3 = "adb shell rm -f /sdcard/%s" % vidname

    def ScreenRecord():
        print("Recording...")
        subprocess.call(cmd, shell=True)


    childp = Process(target=ScreenRecord, args=())

    def signal_handler(signal, frame):
        print("\nRecording was finished.")
        childp.terminate()
        print("Pulling.")
        time.sleep(1)
        subprocess.call(cmd2, shell=True)
        subprocess.call(cmd3, shell=True)
        sys.exit(0)


    def Launcher(arg):
        if len(arg) >= 1:
            if ".mp4" in arg[0]:
                vidname = arg[0]
            else:
                vidname = "%s.mp4" % arg[0]
        genericdir = os.path.join(os.getcwd(), "../videos/")
        if len(arg) == 2:
            if os.path.isdir(arg[1]):
                pulldir = arg[1]
            else:
                print("Directory was not found. Video will be exported to %s" % genericdir)
                if not os.path.isdir(genericdir):
                    os.makedirs(genericdir)
                pulldir = genericdir
 
        childp.start()
        # setting this signal after starting child process to avoid setting it for child too
        # setting for sigint for child will result in calling same method twice.
        signal.signal(signal.SIGINT, signal_handler) 
        print("Starting recording. Press Ctrl+C to finish recording.")
        # lets indicate that it does something...
        cycle = ['-', '/', '|', '\\']
        while True:
            for sym in cycle:
                sys.stdout.write("\b%s" % sym)
                sys.stdout.flush()
                time.sleep(0.1)

