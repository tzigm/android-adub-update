import subprocess
from adb import ADB

#ON MAC OS YOU NEED TO HAVE THE [shell=True] arguments in the subprocess function calls.
#YOU can disable them in WINDOWS OS
def ScreenJob(image_name, folder_path, device):
    filename = "{0}_{1}.png".format(device,image_name)
    full_path = "{0}/{1}".format(folder_path,filename)
    subprocess.call("adb -s %s shell screencap -p /sdcard/%s" % (device,filename),shell=True)
    subprocess.call("adb -s %s pull /sdcard/%s %s" % (device,filename,full_path),shell=True)
    subprocess.call("adb -s %s shell rm /sdcard/%s" % (device,filename),shell=True)
    print("Saved in %s" % folder_path)


def TakeScreenshot(argv):
    image_name = argv[1]
    folder_path = argv[2]
    if image_name == '':
        image_name = "screenshot"
    if folder_path == "":
        folder_path = "~/Desktop"
    debug = ADB()
    devices = debug.devices()
    for device in devices:
        ScreenJob(image_name, folder_path, device)
