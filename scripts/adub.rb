#!/usr/bin/env ruby

# adbwrapper - Wrapper that adds some functionality to the Android Debug Bridge;
#
# adub.rb [global options] <command> [command options] <command> [command options] ...
#
# GLOBAL OPTIONS
#
# * -s <device> option accepts partial ID
# * Last used devices id remembered and used in abscense of -s flag
# * -A : Execute command for all connected devices
#
# COMMANDS
#
# * screen [on/off] : Turn on/off the screen
# * ulist [-u] : Lists all installed Unity packages, optionally uninstalling them
# * ustart <packagename> : starts a Unity app given the package name
# * clearlog - clear logcat
# * log [-v] [-t[<triggerTag>]] [-S<start-time>] [-E<end-time>] [-F] : Fancy logcat
# * cap : Capture screen
# * ins [install options] <apkfile> [-s]  : (Re)install an apk and optionally start it
# * info : Show all info fields about a device
#
# * devs [-L[layout]] [-f] : Detailed device listing
# * check : Interactivly display the names of devices you touch
# * seen [-a] [-o]: Show all seen devices, with online/offline status
# * alias name=id : Add a device alias
# * alias clear : Clear all device aliases
#
# Example
#
# ./adb.rb -A clearlog ins -g test.apk -s log -F
#   For all devices: Clear the log, install and start an APK, and save the logcat to a file

require 'date'
require 'open3'
require 'thread'
require 'fileutils'

$useIConv = false
if RUBY_VERSION.start_with?('1.8')
    require 'iconv'
    $useIConv = true
end

$is_windows = (ENV['OS'] == 'Windows_NT')
$eol = $/

$no_color = false

module Color
    Black = 0
    Red = 1
    Green = 2
    Yellow = 3
    Blue = 4
    Purple = 5
    Cyan = 6
    White = 7
end

################################################################################
# UTILITIES
################################################################################

# Find the path of an executable
def which(cmd)
  exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
  ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each { |ext|
      exe = File.join(path, "#{cmd}#{ext}")
      return exe if File.executable?(exe) && !File.directory?(exe)
    }
  end
  return nil
end

# Shorten string by replacing middle with ellipsis (Modifies argument)
def shorten!(text, l)
    if text.length > l
        l -= 3
        n = text.length - l # Need to remove this many chars around center
        a = l / 2 # Start position
        text[a..(a+n-1)] = '...'
    end
end

# Shorten string by replacing middle with ellipsis
def shorten(text, l)
    return shorten!(text.clone, l)
end

# Print text with color and adjusts to width
def ansi(t, fg = -1, bg = -1, w = 0)


    t = w < 0 ? t.ljust(-w) : t.ljust(w)
    rjust = true
    if w < 0
        rjust = false
        w = -w
    end

    shorten!(t, w) if w > 0

    if $no_color
        if rjust
            return t.rjust(w)
        else
            return t.ljust(w)
        end
    end

    a = fg > 8 || bg > 8 ? 1 : 22
    fg %= 8 if fg > 0
    bg %= 8 if bg > 0
    x = "\x1b[#{a}"
    x += ";#{fg+30}" if fg >= 0
    x += ";#{bg+40}" if bg >= 0
    if rjust
        x += "m#{t.rjust(w)}\x1b[0m"
    else
        x += "m#{t.ljust(w)}\x1b[0m"
    end
    return x
end

# Print out time that has passed since `t` in human readable form
def ptime(t)
    now = Time.now
    d = now - t
    case
    when d < 20
        return "for a few seconds"
    when d < 60
        return "for #{d.to_int} seconds"
    when d < 60*60
        return "for #{d.to_int / (60)} minutes"
    when d < 24*60*30
        if t.day == now.day
            return t.strftime('since %H:%M')
        else
            return t.strftime('since %b %d %H:%M')
        end
    when t.year == now.year
        return t.strftime('since %b %d')
    else
        return t.strftime('since %b %d %Y')
    end

end

# Thread that detects and prints message on stall
class WatchThread < Thread

    def initialize(secs, msg)
        @t = Time.now
        super do
            while @t
                lt = @t
                if lt and Time.now - lt > secs
                    print msg
                    @t = Time.now
                end
                sleep 1
            end
        end
    end

    def finish
        @t = nil
        self.join
    end

end


# Run a block and print a message if the block doesn't finish in time
def onStallPrint(msg = "Stalled...\n", secs = 5)
    watchThread = WatchThread.new(secs, msg)
    yield
    watchThread.finish
end

# Run the provided block in a separate thread for each item in items
def runParalell(items)

    threads = []
    items.each do |d|
        threads << Thread.new do
            yield d
        end
    end
    threads.each(&:join)
end


################################################################################
# ADB
################################################################################

class ADB

    # Static setup

    adbName = $is_windows ? 'adb.exe' : 'adb'
    aaptName = $is_windows ? 'aapt.exe' : 'aapt'

    sysPath = ENV['PATH'].split(File::PATH_SEPARATOR)
    sdkRoot = ENV['ANDROID_SDK_ROOT']
    sysPath.unshift(File.join(sdkRoot, 'platform-tools')) if sdkRoot
    @adbPath = sysPath.map { |p| File.join(p, adbName) }.find { |p| File.exist?(p) }

    sdkRoot = File.expand_path('../..', @adbPath) if sdkRoot.nil? || !File.exist?(sdkRoot)
    @aaptPath = Dir[File.join(sdkRoot.gsub('\\', '/'), 'build-tools', '*', aaptName)].last

    abort("Could not find adb. Make sure it is in your path or ANDROID_SDK_ROOT is set")\
        unless @adbPath
    abort("Could not find aapt. Make sure it is in your path or ANDROID_SDK_ROOT is set")\
        unless @aaptPath

    cacheDir = File.join(ENV['HOME'], '.cache')
    configDir = File.join(ENV['HOME'], '.config')

    Dir.mkdir(cacheDir) unless File.exist?(cacheDir)
    Dir.mkdir(configDir) unless File.exist?(configDir)

    @dev_file = File.join(cacheDir, 'adub_supported_devices')
    @aliasFile = File.join(configDir, 'adub_aliases')

    @deviceDb = {}
    @aliases = {}
    @overrides = {}

    assignment = /^\s*(\w+:)?(\*|\w+)\s*=\s*(?:(?:\"([^"]*)\")|(?:\'([^']*)\')|(\w+))/

    if File.exist?(@aliasFile)
        f = File.open(@aliasFile, 'rb')
        id = nil
        f.each_line do |l|
            if m = assignment.match(l)
                name = m[2]
                val = m[3] || m[4] || m[5]
                if m[1]
                    id = (name == '*' ? id : name)
                    @overrides[id] ||= {}
                    @overrides[id][m[1][0..-2]] = val
                else
                    @aliases[name] = val
                    id = val
                end

            end
        end
        f.close
    end

    csv_file = 'supported_devices.csv'

    if !File.exist?(@dev_file)
        if File.exist?(csv_file)
            f = File.open(csv_file, 'rb:bom|utf-16')

            add_device = Proc.new do |line|
                p = line.strip.split(',')
                model = p[3].tr('_-', ' ')
                device = p[2].tr('_-', ' ')
                name = p[1]
                name = p[0] + ' ' + name if !(name.downcase.start_with?(p[0].downcase))
                @deviceDb[model] = name
                @deviceDb[model + "-" + device] = name
            end

            if $useIConv
                conv = Iconv.new("UTF-8//IGNORE", "UTF-16")
                while (line = f.gets("\x0a\x00"))
                    add_device(conv.iconv(line.strip))
                end
            else
                f.each_line { |l| add_device(l.encode('utf-8')) }
            end
            f.close
            ff = File.open(@dev_file, 'wb')
            Marshal.dump(@deviceDb, ff)
            ff.close
        end
    else
        ff = File.open(@dev_file)
        @deviceDb = Marshal.load(ff)
        ff.close
    end

    def self.saveAliases
        ff = File.open(@aliasFile, 'wb')
        for l in @aliases
            ff.write("#{l[0]}=#{l[1]}\n")
            if @overrides[l[1]]
                for name,val in @overrides[l[1]]
                    ff.write("#{name}:*=\"#{val}\"\n");
                end
            end
        end
        ff.close
    end

    # Represent a registred command the user can run
    class Command
        def initialize(name, args, block)
            @name = name
            @args = args
            @block = block
        end

        def call(args)
            @block.call(args)
        end

        def block
            return @block
        end
    end

    @commands = {}
    @all_commands = {}

    @holder = Object.new

    # Methods

    # Get the path of the adb executable
    def self.adb_path
        return @adbPath
    end

    # Get the path of the aadb executable
    def self.aapt_path
        return @aaptPath
    end

    # Register a command that will be called for each device
    def self.register(name, *args, &block)
        @commands[name] = Command.new(name, args, block)
    end

    # Register a command that will be called once
    def self.registerAll(name, &block)
        @all_commands[name] = Command.new(name, nil, block)
    end

    @modelExtra = {
        # Samsung
        'GT I9000' => 'Galaxy S',
        'GT P5210' => 'Galaxy Tab',
        'GT I9300' => 'Galaxy S3',
        'GT I9082' => 'Galaxy Grand',
        'SGH I337' => 'Galaxy S4',
        'SGH I747' => 'Galaxy S3',
        'SHV E250S' => 'Galaxy Note 2',
        'SC 06D' => 'Galaxy S3',
        'SHV E300K' => 'Galaxy S3',
        'GT N7100' => 'Galaxy Note 2',
        'GT N8000' => 'Galaxy Note 10.1',
        'GT P5200' => 'Galaxy Tab 3 10.1',
        'SC 02B' => 'Galaxy S',
        'SCH I905' => 'Galaxy Tab 10.1',
        'SM G900F' => 'Galaxy S5',
        'SM P600' => 'Galaxy Note 10.1',
        'GT N7105' => 'Galaxy Note 2',
        'SM N910F' => 'Galaxy Note 4',
        'GT I9105P' => 'Galaxy S2+',
        'GT N7000' => 'Galaxy Note',
        'GT P7510' => 'Galaxy Tab 10.1',
        'SM T210' => 'Galaxy Tab 3 7.0',
        'SM T530' => 'Galaxy Tab 4 10.1',
        'SM N900' => 'Galaxy Note 3',
        'SM G920F' => 'Galaxy S6',
        'GT I9100' => 'Galaxy S2',
        'SM P900' => 'Galaxy Note Pro 12.2',
        'SM G800F' => 'Galaxy S5 Mini',
        'GT P7310' => 'Galaxy Tab 8.9',
        'SM G925F' => 'Galaxy S6 Edge',

        # Sony
        'D5303' => 'Xperia T2 Ultra',
        'LT26i' => 'Xperia S',
        'R800x' => 'Xperia PLAY',
        'ST25i' => 'Xperia U',
        'SGP521' => 'Xperia Z2 Tablet',
        'D5503' => 'Xperia Z1',
        'C6603' => 'Xperia Z',
        'C6903' => 'Xperia Z1',
        'D6603' => 'Xperia Z3',
        'D5803' => 'Xperia Z3 Compact',

        # Amazon
        'KFJWI' => 'Kindle Fire HD 8.9" (2012)',
        'KFOT' => 'Kindle Fire (2012)',
        'KFTT' => 'Kindle Fire HD 7" (2012)',
        'KFTBWI' => 'Kindle Fire HD 10 (2015)',
        'KFTHWI' => 'Kindle Fire HDX 7" (2013)',
        'KFFOWI' => 'Fire (2015)',
        'KFMEWI' => 'Fire HD 8 (2015)',
        'KFAPWI' => 'Kindle Fire HDX 8.9" (2013)',
        'SD4930UR' => 'Fire Phone',

        # LG
        'LG F180L' => 'Optimus G',
        'LG F200K' => 'Optimus VU 2',
        'LG D802' => 'G2',
        'LG P700' => 'Optimus L7',
        'LG H815' => 'G4',
        'LG E460' => 'Optimus L5 II',
        'LG D855' => 'G3',

        # Other
        'ASUS T00I' => 'Zenfone 4',
        '840FHD' => 'Iconia Tab 8',
        'A200' => 'Iconia Tab',
        'S510' => 'Liquid S1',
        'A1 840FHD' => 'Iconia Tab 8',
        'Yellowstone' => 'Project Tango',
        'K013' => 'MeMO Pad 7',
        'PG86100' => 'HTC EVO'
    }
    @brandMap = { "SEMC" => "Sony Ericsson" }

    # Return an user readable name of the device. Uses the supported
    # devices database if available.
    def self.formatName(d)

        brand = d['brand'].tr('_', ' ')
        model = d['model'].tr('_-', ' ')
        device = d['device']

        if device
            x =  @deviceDb[model + '-' + device]
        else
            x =  @deviceDb[model]
        end

        if !x
            brand = @brandMap[brand] if @brandMap[brand]
            brand[0] = brand.split('')[0].upcase if brand.length > 0
            model = model.gsub('SAMSUNG ', '')
            extra = @modelExtra[model]
            model[0] = model.split('')[0].upcase if model.length > 0
            x = brand + " " + model
            if extra
                x = x + " (" + extra + ")"
            end
            x = x + "*"
        end
        return x
    end

    def self.getprop(id, prop)
        return shell(id, "getprop #{prop}").rstrip
    end

    def self.getDevices(full = false, fingerPrint = false)

        device_hash = File.join(ENV['HOME'], '.adb_devices')
        Dir.mkdir(device_hash) if !File.exist?(device_hash)

        rc = []

        lines = `"#{@adbPath}" devices`.split("\n")[1..-1]

        # Get rid of phones with same IDs (Bad, bad phones)
        lines = lines.select { |l| lines.count(l) == 1 }

        return if !lines

        utre = /[^\d]*((\d+) days[^\d]*)?(\d+):(\d+):(\d+)/
        batre = /^\s*level: (\d+)/
        dispre = /(DisplayWidth=(\d+)\s+DisplayHeight=(\d+)|init=(\d+)x(\d+))/
        glesre = /GLES: ([^\,]*)\, ([^\,]*)\, ([^\n]*)/

        runParalell(lines) do | line |
            l = line.split(" ")

            next if l[1] != 'device' # TODO: 'unauthorized' / 'offline'

            getAll = full
            dev_name = File.join(device_hash, l[0])

            if File.exist?(dev_name)
                FileUtils.touch(dev_name)
                h = File.open(dev_name, "r") { |f| Marshal.load(f) }
            end
            if !h or !h['model'] or h['model'] == '' or !h['gpubrand']
                getAll = true
                h = Hash[ l[2..-1].map { |x| x.split(":") } + [['id', l[0]]]  ]
            end

            onStallPrint(l[0] + " stalled...\n", 5) {

                h['device'] ||= getprop(l[0], "ro.product.device")
                h['model'] ||= getprop(l[0], "ro.product.model")
                h['cpu'] ||= getprop(l[0], "ro.product.cpu.abi")
                h['brand'] ||= getprop(l[0], "ro.product.brand")
                h['buildtime'] = getprop(l[0], "ro.build.date")
                h['fingerprint'] = getprop(l[0], "ro.build.fingerprint")
                h['version'] = getprop(l[0], "ro.build.version.release")

                h['battery'] = -1
                if getAll
                    m = utre.match(shell(l[0], "uptime"))
                    if m
                        _,days,hours,mins,secs = m.captures.map(&:to_i)
                        h['uptime'] = secs + mins*60 + hours*3600 + (m.captures[0] ? days*3600*24 : 0)
                    end
                    m = batre.match(shell(l[0], "dumpsys battery"))
                    if m
                        h['battery'] = m.captures[0].to_i()
                    end
                    m = dispre.match(shell(l[0], "dumpsys window"))
                    if m
                        if m.captures[1] and m.captures[1] != ''
                            h['width'] = m.captures[1].to_i()
                            h['height'] = m.captures[2].to_i()
                        else
                            h['width'] = m.captures[3].to_i()
                            h['height'] = m.captures[4].to_i()
                        end
                    end
                    m = glesre.match(shell(l[0], "dumpsys SurfaceFlinger"))
                    h['gles'] = h['gpubrand'] = h['chipset'] = ''
                    if m
                        h['gpubrand'] = m.captures[0] if m.captures[0]
                        h['chipset'] = m.captures[1] if m.captures[1]
                        h['gles'] = m.captures[2] if m.captures[2]
                    end

                end
            }

            if getAll
                File.open(dev_name, "w") { |f| Marshal.dump(h, f) }
            end

            if l[0] && @overrides[l[0]]
                for name,val in @overrides[l[0]]
                    h[name] = val
                end
            end

            rc << h
        end

        return rc

    end

    def self.shell(id, args)
        tries = 30
        result = ''
        Open3.popen3(@adbPath, "-s", id, "shell", *args) do |i, o, e|
            tries -= 1
            result = o.read
            er = e.read
            # Hack -- need to differ between ADB errors and actual command errors
            return er if er.include? "denied"
            redo if tries > 0 && e.read.length > 0
        end
        return result
    end

    def self.exec_out(id, args)
        tries = 30
        result = ''
        Open3.popen3(@adbPath, "-s", id, "exec-out", *args) do |i, o, e|
            tries -= 1
            result = o.read
            redo if tries > 0 && e.read.length > 0
        end
        return result
    end

    def self.print(s)
        Kernel.print s
    end

    # RUN

    def self.run_(id, args)

        found = false
        while i = args.find_index { |a| a[0].chr != '-' }
            found = true
            if cmd = @commands[args[i]]
                args = args[i+1..-1]
                as = ADBSession.new(id, @holder, args)
                as.eval(cmd.block, args)
                as.output.each { |o| yield o }
            else
                args = args[i..-1]
                if args[0] == 'shell' and args.length == 1
                    runInteractive(id, args)
                else
                    runAdb(id, args) { |o| yield o }
                end
                break
            end
        end
        if not found
            runAdb(id, args) { |o| yield o }
        end
    end

    def self.run(id, args)
        if block_given?
            run_(id, args) { |l| yield l }
        else
            run_(id, args) { |l| puts l }
        end
    end


    def self.runAdb(id, args)

        if id
            args = args.clone
            args.unshift(id)
            args.unshift('-s')
        end

        success = false
        while not success
            Open3.popen3(@adbPath, *args) do | stdin, stdout, stderr, pid |
                failed = false
                t = Thread.new do
                    #p "THREAD"
                    begin
                        stderr.each_line($eol) do |l|
                            if l.start_with?('error')
                                failed = true
                                break
                            end
                            print l
                        end
                    rescue
                        p "CLOSED"
                    end
                end
                stdout.each_line($eol) do |line|
                    break if failed
                    yield line if block_given?
                end
                #p "JOIN"
                t.join
                success = !failed
            end
        end
    end

    def self.runInteractive(id, args)
        system(@adbPath, "-s", id, *args)
    end

    def self.grep
        return @grep
    end

    def self.skip
        return @skip
    end

    #attr_reader :args
    def self.args
        return @args
    end

    def self.getOpt(name)
        rv = nil
        @args.delete_if { |a|
            px = a[0..1]
            next (rv = a[2..-1]) if px == name
        }
        return rv
    end

    # Execute an ADB commandline. Main entry point
    def self.exec(args)
        @skip = /^(EntryPointNotFoundException|OPENGL DEBUG)/
        @grep = nil
        #if gflag = args.index("-g")
        #    @grep = args[gflag+1]
        #    args.delete_at(gflag)
        #    args.delete_at(gflag)
        #end

        skip_commands = [ 'start-server', 'kill-server', 'connect', 'disconnect' ]

        firstCommandAt = 99
        $no_color = true if !$stdout.isatty && !$is_windows

        if nc = args.index("-nc")
            $no_color = true
            args.delete_at(nc)
        end

        # Check for non device command first
        if i = args.find_index { |a| a[0].chr != '-' }

                firstCommandAt = i

                if skip_commands.include?(args[i])
                    runAdb(nil, args) { |l| puts l }
                    return
                end

            if cmd = @all_commands[args[i]]
                @args = args = args[i+1..-1]
                self.instance_exec args, &cmd.block
                return
            end
        end

        if aflag = args.index("-A") and aflag < firstCommandAt
            args.delete_at(aflag)
            runAll(args)
            return
        end

        # Device command, deal with -s flag
        id = nil

        # Load last id
        last_adb_name = File.join(ENV['HOME'], '.last_adb')
        begin
            id = File.open(last_adb_name, 'rb').read
        rescue
        end

        devices = getDevices
        if devices.length > 0
            if sflag = args.index("-s") and sflag < firstCommandAt
                id = args[sflag+1]

                args.delete_at(sflag)
                args.delete_at(sflag)

                id = @aliases[id] if @aliases[id]

                dev = devices.find { |d| d['id'].start_with?(id) }

                if dev
                    id = dev['id']
                    File.open(last_adb_name, 'wb').write(id)
                end
            else
                dev = devices.find { |d| d['id'] == id }
                if not dev
                    dev = devices[0]
                    id = dev['id']
                    File.open(last_adb_name, 'wb').write(id)
                end
                print "#{ansi(ADB.formatName(dev) + ' (' + dev['id'] + ')', Color::Yellow)}\n"
            end
        end

        run(id, args)
    end

    def self.forAllDevices(devices = nil)

        devices = getDevices(false) unless devices
        inProgress = []

        t = Time.now
        watchTread = Thread.new do
            while t
                if Time.now - t > 10
                    if inProgress.length <= (devices.length+1)/2 and inProgress.length > 0
                        inProgress.each { |d| print "#{ADB.formatName(d)} (#{d['id']}) still busy...\n" }
                    end
                    t = Time.now
                end
                sleep 1
            end
        end

        runParalell(devices) { |d|
            inProgress << d
            yield d
            inProgress.delete(d)
        }

        t = nil
        watchTread.join
    end

    def self.runAll(args)
        forAllDevices { |d|
            lines = []
            ADB.run(d['id'], args.clone) { |l| lines << l }
            lines = lines.select { |l| l.include?(@grep) } if @grep
            output = lines.join()
            print "\n#{ansi(ADB.formatName(d) + ' (' + d['id'] + ')', Color::Yellow)}\n#{output}\n"
        }
    end

    def self.runSome(args, devs)
        forAllDevices { |d|
            if devs.include? d['id']
                lines = []
                ADB.run(d['id'], args.clone) { |l| lines << l }
                lines = lines.select { |l| l.include?(@grep) } if @grep
                output = lines.join()
                print "\n#{ansi(ADB.formatName(d) + ' (' + d['id'] + ')', Color::Yellow)}\n#{output}\n"
            end
        }
    end

end ## ADB CLASS


# An instace of this class is created for each device to call. It is the context
# from which registered commands are called.
class ADBSession

    @@ft_mutex = Mutex.new

    def initialize(id, holder, args)
        @device_id = id
        @output = []
        @holder = holder
        @args = args
    end

    attr_reader :device_id
    attr_reader :output
    attr_reader :args

    def shell(args)
        return ADB.shell(@device_id, args)
    end

    def exec_out(args)
        return ADB.exec_out(@device_id, args)
    end

    # Get a command line option. Will return nil if not present
    def getOpt(name)
        rv = nil
        @args.delete_if { |a|
            px = a[0..1]
            next (rv = a[2..-1]) if px == name
        }
        return rv
    end

    def run(args)

        return ADB.runAdb(@device_id, args)
    end

    # Context specific print. Will be queued and output together after the
    # command is finished
    def print(s)
        @output << s
    end

    def eval(b, args)
        self.instance_exec args, &b
    end

    # Introduce a block of code that is only run once
    def firstTime(&b)
        @@ft_mutex.synchronize {
            if @holder.instance_variable_defined?(:@beenHere)
                next
            end
            @holder.instance_variable_set(:@beenHere, true)
            b.call
        }
    end

end

##########################################################################################
# REGISTERED COMMANDS
#
# Commands are registered through the static `register` and `registerAll` methods.
#
# `register` is called once for each device, while `registerAll` is only called once.
#
# The block is executed when the command is encountered in the command line, with `args`
# containing the rest of the command line (not including the command) and the code is
# expected to remove all argument it uses, so that several commands can be entered in
# sequence.
#
# Call `print()` for delayed output that is sorted under a device specific heading.
#
# `device_id` is set to the current id.
#
# A `register` block can use `firstTime do` to have a code block that only executes once.
#
##########################################################################################

ADB.register("dump") { print(shell("dumpsys")) }

ADB.registerAll("test") {
    puts getOpt("-t")
}

ADB.registerAll("forget") {

    d = getOpt('-d')
    next if !d || (d == '')
    beforeDate = DateTime.parse(d)

    print("Forgetting devices beore #{beforeDate.to_s}\n")

    Dir[File.join(ENV['HOME'].gsub('\\', '/'), ".adb_devices/*")].each { |path|
        mod = File.mtime(path).to_date
        if mod < beforeDate
            print("Deleting #{path}\n")
            File.delete(path)
        end
    }
}


ADB.register("ustart", "<package>") {
    pkg = args.shift
    res = nil

    [ 'UnityPlayerActivity', 'UnityPlayerNativeActivity' ].each do |activity|
        res = shell("am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -f 0x10200000 -n #{pkg}/com.unity3d.player.#{activity}")
        break if ! res.include?('Error type 3')
    end
    print(res)
}

ADB.register("cap") {
    shell("screencap -p /sdcard/screen.png")
    File.delete("#{device_id}.png") if File.exist?("#{device_id}.png")
    run(["pull",  "/sdcard/screen.png", "#{device_id}.png"])
    shell("rm /sdcard/screen.png")
    if getOpt('-o')
        if $is_windows
            system("start #{device_id}.png")
        else
            system("open #{device_id}.png")
        end
    end
}

ADB.register("screen", "on|off") {

    what = args.length > 0 ? args.shift : ''
    doOn = what == 'on'
    doOff = what == 'off'

    re = /.*(?:mScreenOn|mPowerState|Display Power: state)=(\w+).*/
    state = -1
    shell("dumpsys power").each_line($eol) do |l|
        if m = re.match(l)
            state = (['false', 'OFF', '0'].include? m.captures[0]) ? 0 : 1
            break
        end
    end

    if (doOn and state == 0) or (doOff and state == 1)
        print("Turning screen #{doOn ? "on" : "off"}\n")
        shell("input keyevent 26")
        if doOn
            shell("input keyevent 89")
        end
    else
        print("Screen state: #{["Unknown", "Off", "On"][state+1]}")
    end
}

ADB.register("ulist", "[-u]") {

    dontRemove = ["com.motorola.", "com.amazon", "com.htc.", "com.sony", "android", "com.google.",
                   "com.android.", "com.sec.", "com.samsung", "com.nvidia.", "com.facebook.",
                   "com.oculus.", "com.qualcomm.", "com.unity3d.DeviceManager", ]
    uninstall = false
    if (args[0] == '-u')
        args.shift
        uninstall = true
    end

    shell("pm list packages -f").each_line($eol) do | line |
        if m = /package:(.*)=([^\r\n]*)/.match(line)
            _apk,package = m.captures
            if !dontRemove.find { |prefix| package.start_with?(prefix) }
                libs = shell("ls /data/data/#{package}/lib/")
                if libs.include?('denied')
                    dump = shell("pm dump #{package} | head -n 40")
                    isUnity = dump =~ /#{package}\/.*UnityPlayer/
                else
                    isUnity = libs.include?('libunity.so')
                end

                if isUnity
                    if uninstall
                        print("Uninstalling " + package + "\n")
                        run(['uninstall', package])
                    else
                        print(package + "\n")
                    end
                end
            end
        end
    end
}

ADB.registerAll("devs") {

    print("List of devices\n")

    fingerPrint = full = false
    raw = false;
    layoutName = nil

    args.delete_if { |a|
        px = a[0..1]
        next (full = true) if a == '-l'
        next (fingerPrint = true) if a == '-f'
        next (raw = true) if a == '-r'
        next (layoutName = a[2..-1]) if px == '-L'
        break
    }
    full = true if layoutName
    layoutName = 'default' if layoutName.nil? || layoutName.empty?

    devs = getDevices(full, fingerPrint).sort {|d0,d1| formatName(d0) <=> formatName(d1) }

    break if not devs

    layoutList = { 'default' =>
                   "id:-20:7,cpu:12:1,gpu:20:5,screen:9:6,os:7:2,bat:-4:5,up:-5:4,name:32:7",
                   'vilnius' =>
                   "alias:5:4,id:-20:7,cpu:12:1,gpu:20:5,screen:9:6,os:7:2,name:32:7",
                   'graphics' =>
                   "id:-20:7,cpu:12:1,gpu:20:5,screen:9:6,os:7:2,gles:20:3,name:32:7"
    }

    layout_txt = layoutList[layoutName] || layoutName

    layout = layout_txt.split(",").map { |p| p.split(":") }.map do |txt,len, col|
        [txt, len.to_i, col ? col.to_i : 7]
    end

    if full and !raw
        layout.each do |txt,len,col|
            len = -len if len < 0
            print("#{ansi(txt.upcase.ljust(len), Color::Black, col)} ")
        end
        print("\n")
    end

    invAlias = @aliases.invert

    devs.each do |d|
        d['name'] = formatName(d)
        d['os'] = d['version'].gsub('Lollipop', '5.0 ')
        d['up'] = d['uptime'] ? ((d['uptime']+1799)/3600).to_s + 'h' : '??h'
        d['bat'] = d['battery'].to_s + '%'
        d['screen'] = d['width'].to_s+'x'+d['height'].to_s
        cs = d['chipset'] or '?'
        if cs.start_with?('Intel(R) HD Graphics for ')
            cs = "Intel/" + cs[25..-1]
        elsif cs.start_with? "GC1000 "
            cs = "Vivante GC1000"
        end
        d['gpu'] = cs
        d['alias'] = (invAlias[d['id']] or '')

        if full
            if raw
                layout.each {|txt,| print("#{d[txt]}\t") }
            else
                layout.each do |txt,len,col|
                    if len < 0
                        print("#{ansi(d[txt].rjust(-len),col,0)} ")
                    else
                        print("#{ansi(d[txt].ljust(len),col,0)} ")
                    end
                end
            end
        else
            if raw
                print("#{d['id']}\t#{d['cpu']}\t#{d['name']}")
            else
                print("#{d['id'].rjust(20)} #{ansi(d['cpu'].ljust(11), Color::Red)} #{d['name']}")
            end
        end

        if fingerPrint
            if raw
                print("\t#{d['fingerprint']}\n")
            else
                print("\n#{d['fingerprint']}\n")
            end
        else
            print("\n")
        end
    end
    print("=> #{devs.length} devices\n")
}

ADB.registerAll("alias") {
    if args.length == 0
        for l in @aliases
            print "#{l[0]}=#{l[1]}\n"
        end
    elsif args.length == 1 and args[0] == '-C'
        @aliases = {}
        ADB.saveAliases
    else

        for a in args
            var,val = a.split('=')
            next if not val
            val.strip!
            real = nil
            getDevices.each { |d|
                real = d['id'] if d['id'].start_with?(val)
            }
            if not real
                real = val if File.exist?(File.join(ENV['HOME'], ".adb_devices", val))
            end

            if real
                @aliases[var.strip] = real
            else
                print "No such device\n"
            end
        end
        ADB.saveAliases()
    end
}

ADB.registerAll("seen") {
    devhash = {}
    devices = getDevices
    devices.each { |d| devhash[d['id']] = true }
    showOther = showAlias = flagSet = false

    args.delete_if { |a|
        next (flagSet = showAlias = true) if a == '-a'
        next (flagSet = showOther = true) if a == '-o'
        break
    }

    if not flagSet
        showOther = showAlias = true
    end

    if showAlias and @aliases.length > 0
        print "Aliased Devices\n"
        for name,id in @aliases
            dev = devices.find { |d| d['id'] == id }
            path = File.join(ENV['HOME'], ".adb_devices", id)
            if File.exist? path
                mod = File.mtime(path)
                t = ptime(mod)
                #d = File.open(path, 'r') { |ff| Marshal.load(ff) }
                print "#{name} (#{id})"
                print dev ? " #{ansi("ONLINE", Color::Green)}\n" : " #{ansi("OFFLINE", Color::Red)} (#{t})\n"
            else
                print "#{name} (#{id}) #{ansi("UNSEEN", Color::Yellow)}\n"
            end
        end
        print "\n"
    end

    if showOther
        print "Other devices\n"
        sorted = Dir[File.join(ENV['HOME'].gsub('\\', '/'), ".adb_devices/*")].each.sort_by {|f| File.mtime(f)}
        sorted.each do |f|
            mod = File.mtime(f)
            t = ptime(mod)
            d = File.open(f, "r") { |ff| Marshal.load(ff) }
            if d and d['model'] and d['model'].length > 0
                name = formatName(d)
                print "#{d['id'].rjust(20)} #{ansi(d['cpu'].ljust(11),Color::Red)} #{name}"
                print devhash.has_key?(d['id']) ?
                    " #{ansi("ONLINE", Color::Green)}\n" : " #{ansi("OFFLINE", Color::Red)} (#{t})\n"
            end
        end
    end
}

ADB.register("info") {
    dev = ADB.getDevices.find { |d| d['id'] == device_id }
    if not dev and File.exist?(path =File.join(ENV['HOME'], ".adb_devices", device_id))
        dev = File.open(path, "r") { |f| Marshal.load(f) }
    end
    if not dev
        print("No such device\n")
    else
        for name,val in dev
            print("#{name.upcase} : #{val}\n")
        end
    end
}

ADB.registerAll("check") {
    puts "Interact with device to see its name..."

    runParalell(getDevices) do |d|
        id = d['id']
        run(id, ["shell", "getevent"]) do |l|
            x = l.split(" ")
            if x.length >= 4
                what = x[1].to_i(16)
                a0 = x[2].to_i(16)
                if what == 1 or (what == 3 and a0 > 0x30)
                    puts "#{ansi(ADB.formatName(d), Color::Yellow)} (#{id})"
                end
            end
        end
    end
}

ADB.register("un", "<apk> [apk]...") {
    args.each do |a|
        run(["uninstall", a])
    end
    args.clear
}

ADB.register("ins", "<apk>", "[-s]") {

    apk = nil
    legalOptions = [ '-l', '-r', '-t', '-s', '-d', '-g' ]

    options = [ '-r' ]
    option = ''
    illegalOption = nil

    while not apk
        option = args.shift
        break if not option
        legal = legalOptions.include? option
        options << option
        if option[0] == '-'
            illegalOption = option if (not legal) and (not illegalOption)
        else
            apk = option
        end
    end

    if not apk or illegalOption
        if illegalOption
            print "Invalid option '#{illegalOption}'\n"
        else
            print "No <apk> specified\n"
        end
        next
    end

    firstTime do
        pkg_pattern = /\s*A: package(?:\([^\)]*\))?="([^"]*)"/
        act_pattern = /\s*A: android:name(?:\([^\)]*\))?="([^"]*)"/
        inActivity = false
        mainActivity = nil
        activity = package = nil
        `#{ADB.aapt_path} dump xmltree #{apk} AndroidManifest.xml`.each_line do |l|

            l = l.lstrip
            if l.start_with?('E: ')
                if inActivity
                    if l[3..15] == 'intent-filter'
                        mainActivity = activity
                    end
                end
                inActivity = (l[3..10] == 'activity')
            end

            if m = pkg_pattern.match(l)
                package = m.captures[0]
            end
            if inActivity and m = act_pattern.match(l)
                activity = m.captures[0]
            end
        end

        if mainActivity
            activity = mainActivity
        end

        if !activity or !package
            puts "* Could not parse Manifest. No working aapt in path?"
        end

        $activity = activity
        $package = package
    end

    print "Installing #{$package}/#{$activity}\n"
    run(["install"] + options)

    if args[0] == '-s' and $package and $activity
        args.shift
        print shell("am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -f 0x10200000 -n #{$package}/#{$activity}")
    end
}

ADB.register("start") {
    print shell("am start -a android.intent.action.MAIN -c android.intent.category.LAUNCHER -f 0x10200000 -n #{$package}/#{$activity}")
}

ADB.register("clearlog") {
    run(["logcat", "-c"])
}

ADB.register("log", "-v", "-t<grepTag>") {

    level = '*:D'
    grepTag = nil
    excludeTags = []
    startTime = nil
    endTime = nil
    dumpFile = nil
    doOutput = true
    follow = true

    args.delete_if { |a|
        px = a[0..1]
        next (excludeTags = a[2..-1].split(",")) if px == '-x'
        next (level = '*:V') if a == '-v'
        next (level = '*:E') if a == '-e'
        next (level = '*:W') if a == '-w'
        next (grepTag = a[2..-1]) if px == '-t'
        next (startTime = a[2..-1]) if px == '-S'
        next (endTime = a[2..-1]) if px == '-E'
        next (dumpFile = a[2..-1]) if px == '-F'
        break
    }

    if startTime
        startTime = DateTime.parse(startTime)
    end
    if endTime
        if endTime == ''
            endTime = DateTime.now
        else
            endTime = DateTime.parse(endTime)
        end
    end

    if dumpFile
        dumpFile = File.open(device_id + ".logcat", 'w')
        doOutput = false
        follow = false
    end

    grepTag = 'Unity' if grepTag == ''


    firstTime do
        $id_counter = -1
        $lines = Queue.new
        $logThread = Thread.new do
            linecol = { "V" => Color::White, "D" => Color::White, "I" => Color::Yellow, "W" => Color::Red, "E" =>Color::Red, "F" => Color::Purple }
            loop do
                id,ts,pid,_tid,dl,tag,lines = $lines.pop
                c = linecol[dl];
                pc = ((pid^0x12345) % 6 ) + 1
                if $id_counter > 0
                    lines.each_with_index { |l,i|
                        puts i > 0 ? "#{''.ljust(42)} #{ansi(l, c, -1, 0)}" :
                            " #{ansi(id, Color::Green, -1, -10)}  #{ansi(tag, pc, -1, 20)} "\
                            "#{ts.strftime("%T")} #{ansi(l, c, -1, 0)}"
                    }
                else
                    lines.each_with_index { |l,i|
                        puts i > 0 ? "#{''.ljust(29)} #{ansi(l, c, -1, 0)}" : "#{ansi(tag, pc, -1, 20)} #{ts.strftime("%T")} #{ansi(l, c, -1, 0)}"
                    }
                end
                STDOUT.flush
            end
        end
    end

    logTime = nil

    stdout = IO.popen("#{ADB.adb_path} -s #{device_id} logcat #{follow ? '' : '-d'} -v long #{level}")
    year = DateTime.now.year
    rx = /\[\s*(\d+)-(\d+)\s+(\d+):(\d+):(\d+).(\d+)\s+(\d+):\s*(\d+)\s+(\w)\/(.*?)\s+\]/
    first = true
    tstamp,pid,tid,dl,tag = nil
    text = []
    logPid = -1

    while true
        begin
            line = stdout.readline() #_nonblock()
            if $useIConv
                line = Iconv.iconv('UTF-8', 'iso8859-1', line)[0].rstrip
            else
                line = line.encode('UTF-8','iso8859-1').rstrip
            end
            if line == ""
                first = true
            elsif first
                if m = rx.match(line)
                    if text.length > 0
                        if dumpFile
                            text.each_with_index { |l,i|
                                outline = i > 0 ? "#{''.ljust(29)} #{l}\n" : "#{tag.ljust(20)} #{tstamp.strftime("%T")} #{l}\n"
                                dumpFile.write(outline)
                            }
                        end
                        if doOutput
                            $lines << [device_id, tstamp, pid, tid, dl, tag, text]
                        end
                        logTime = Time.now
                        text = []
                    end

                    pid,tid = m.captures[6..8].map(&:to_i)
                    dl,tag = m.captures[8..10]
                    tag = tag.strip
                    tstamp = DateTime.new(year, *m.captures[0..5].map(&:to_i))
                    if endTime and endTime <= tstamp
                        break
                    end

                    if tag == grepTag
                        logPid = pid
                    end

                    first = false
                else
                    text << line if text.length > 0
                end
            elsif !first
                shouldLog = (!grepTag || logPid == pid) && !excludeTags.include?(tag)
                shouldLog &&= !(ADB.skip && ADB.skip.match(line))
                shouldLog &&= (!ADB.grep || tag.include?(ADB.grep) || line.include?(ADB.grep))
                shouldLog &&= (!startTime || tstamp > startTime)
                text << line if shouldLog
                first = true
            end
        rescue EOFError
            break
        rescue
            sleep(0.1)
        end
    end
    if dumpFile
        print "Log for #{device_id} saved\n"
        dumpFile.close()
    end
}

###################################################################
# MAIN
###################################################################

Thread::abort_on_exception = true
ADB.exec(ARGV)

