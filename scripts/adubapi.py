from multiprocessing import Process
import subprocess
import sys
import os
from adubhelp import CmdInfo
from screenrecord import Recorder

try:
    # for Python2
    from Tkinter import *
except ImportError:
    # for Python3
    from tkinter import *

# try launching any command which is calling adub.rb from non adubapi.py directory
# MacOS fails launching it (adub.rb not found), does Windows work?
scriptpth = os.path.abspath(os.path.dirname(sys.argv[0]))
s_adbrbPath = "%s/adub.rb" % scriptpth
s_tabParsePath = "%s/tabParse.py" % scriptpth
s_screenPath = "%s/screen.py" % scriptpth # will have to change this and call this script directly.

def ExecuteCommand(commands):
    command_result = subprocess.call(commands, shell=True)
    return command_result

def ExecutePipe(commands):
    command_result = ''
    results = os.popen(commands, "r")
    while 1:
        line = results.readline()
        if not line:
            break
        command_result += line
    return command_result


def ExecAlog():
    print(CmdInfo.alog)



def ExecD(args):
    # first two arguments were stripped (command name "adubapi" and adub command name "d")
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.d)
        elif args[0] == "-m":
            ExecuteCommand("ruby %s devs -r" % s_adbrbPath)
        else:
            print("Argument was not found. To see what this command can do, use \"d -help\".")
    elif len(args) > 1:
        print("Too many arguments. To see what this command can do, use \"d -help\".")
    else:
        ExecuteCommand("ruby %s devs -l" % s_adbrbPath)



def ExecDc(args):
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.dc)
        else:
            print("Argument was not found. To see what this command can do, use \"d -help\".")
    elif len(args) > 1:
        print("Too many arguments. To see what this command can do, use \"dc -help\".")
    else:
        buffer = ""
        buffer = ExecutePipe("ruby %s devs -l -f -r | python %s" % (s_adbrbPath, s_tabParsePath))
        # for some reason MacOS can't save data to clipboard via Tkinter lib
        # working around with subprocess library, will see if that works
        # if it would work for MacOS - test on Windows. If windows will work with it too, I will remove Tk
        if sys.platform == "darwin":
            pr = subprocess.Popen('pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
            pr.communicate(buffer.encode('utf-8'))
            print("Copied for MACOS")
        elif sys.platform == "win32":
            clpb = Tk()
            clpb.withdraw()
            clpb.clipboard_clear()
            clpb.clipboard_append(buffer)
            clpb.update() # now it stays on the clipboard after the window is closed
            clpb.destroy()
            print("Copied for WINOS")

        print("Copied device information to clipboard.")


def ExecIns(args):
    if len(args) == 0:
        print("This command requires some parameters. Please check \"ins -help\".")
    if args[0] == "-help":
        print(CmdInfo.ins)
        return

    apkPath = args[0]
    globalVars = ""
    additionalParam = ""
    for i in range(1, len(args)):
        if args[i] == "-A":
            globalVars = "-A"
        elif args[i] == "-S":
            if args[i + 1]:
                i = i + 1
                globalVars = "-S %s" % args[i]
            else:
                print("You tried installing using device ID (-S parameter) but you forgot to include ID.")
        elif args[i] == "-s":
            additionalParam = "-s"

    ExecuteCommand("ruby %s %s ins %s %s" % (s_adbrbPath, globalVars, apkPath, additionalParam))


def ExecList(args):
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.list)
        elif args[0] == "-A":
            ExecuteCommand("ruby %s -A ulist" % s_adbrbPath)
        else:
            print("Argument was not found. To see what this command can do, use \"list -help\".")

    elif len(args) == 2:
        if args[0] == "-S":
            ExecuteCommand("ruby %s -S %s ulist" % (s_adbrbPath, args[1]))

    elif len(args) == 0:
        ExecuteCommand("ruby %s ulist" % s_adbrbPath)
    else:
        print("Too many arguments. To see what this command can do, use \"list -help\".")


def ExecPurge(args):
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.purge)
        elif args[0] == "-A":
            ExecuteCommand("ruby %s -A ulist -u" % s_adbrbPath)
        else:
            print("Argument was not found. To see what this command can do, use \"purge -help\".")

    elif len(args) == 2:
        if args[0] == "-S":
            ExecuteCommand("ruby %s -S %s ulist -u" % (s_adbrbPath, args[1]))

    elif len(args) == 0:
        ExecuteCommand("ruby %s ulist -u" % s_adbrbPath)
    else:
        print("Too many arguments. To see what this command can do, use \"purge -help\".")


def ExecRecord(args):
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.record)
        else:
            print("Argument was not found. To see what this command can do, use \"record -help\".")
    else:
        Recorder.Launcher(args)

def ExecScreen(args):
    imageName = "screenshot"
    dir = os.path.join(os.getcwd(), "../screenshots/")
    if len(args) >= 1:
        if args[0] == "-help":
            print(CmdInfo.screen)
            return
        else:
            imageName = args[0]
        if len(args) >= 2:
            if os.path.isdir(args[1]):
                dir = args[1]
            else:
                print("Your given directory was not found. Video will be saved in %s" % dir)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    from screen import TakeScreenshot
    TakeScreenshot([s_screenPath, imageName, dir])


def ExecSeen(args):
    if len(args) == 1:
        if args[0] == "-help":
            print(CmdInfo.seen)
        else:
            print("Argument was not found. To see what this command can do, use \"purge -help\".")
    else:
        print("Too many arguments. To see what this command can do, use \"purge -help\".")

    ExecuteCommand("ruby %s seen" % s_adbrbPath)


if __name__ == '__main__':

    if sys.argv[1] == "adub":
        # directly printing as it wont have any other purpose
        print(CmdInfo.adub2)
    elif sys.argv[1] == "alog":
        ExecAlog()
    elif sys.argv[1] == "d":
        ExecD(sys.argv[2:])
    elif sys.argv[1] == "dc":
        ExecDc(sys.argv[2:])
    elif sys.argv[1] == "ins":
        ExecIns(sys.argv[2:])
    elif sys.argv[1] == "list":
        ExecList(sys.argv[2:])
    elif sys.argv[1] == "purge":
        ExecList(sys.argv[2:])
    elif sys.argv[1] == "record":
        ExecRecord(sys.argv[2:])
    elif sys.argv[1] == "screen":
        ExecScreen(sys.argv[2:])
    elif sys.argv[1] == "seen":
        ExecSeen(sys.argv[2:])
    else:
        print("Command send via parameter was not found.")
