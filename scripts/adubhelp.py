# Basically, this file stores information about the commands. 
# When adding or changing any adub command, don't forget to update this data
# Also add new command to adub string, as it shows all available commands

import sys

class CmdInfo(object):

    adub = """
    === 	adub command list and usage 	===
    === [COMMAND] [VARS] 	- [Description] ===

    d 			- Display connected devices and their information
    dc 			- Copy connected devices information to clipboard for easy pasting
    ins apk_path device -s - Install an apk from given path to given device (-A for all devices) 
    ===  ===  ===  ===  === and will start the app if -s is present
    list 			- List all apps installed on the device (Ignores official apps, oculus files etc)
    purge 			- Delete all apps that are listed in 'list' command
    record name		- Record a video to 'Videos/name.mp4'. CTRL + C to stop recording. 
    ===  ===  ===  ===  === Enter N when asked about batch jobs 
    screen name device	- Take a screenshot of a connected device and put in 'Screenshots' folder 
    seen 			- List all devices that were ever connected to the computer
    ===  ===  ===  ===  ===  ===  ===  ===  ==="""

    adub2 = """
    Adub command list and usage.
    Here is list of commands in shell files. Make sure to create proper aliases
    ---------------------------------------------------------------------------
    d       - Display connected devices and their information.
    dc      - Copy connected devices information to clipboard for easy pasting.
    ins     - Install an apk for device. This command has advanced options, please check \"ins -help\".
    list    - List all apps installed on the device (Ignores official apps, oculus files etc).
    purge   - Delete all apps that are listed in 'list' command.
    record  - Record a video of device's screen. CTRL + C to stop recording.
    screen  - Take a screenshot of a connected device and put in 'Screenshots' folder.
    seen    - List all devices that were ever connected to the computer
    ---------------------------------------------------------------------------
    In production:
    alog    - Clears and gets logcat from device. Same as adb logcat -c; adb logcat -v threadtime command combination.
    ---------------------------------------------------------------------------
    To see more detailed information about command use \"adubhelp [command]\" or \"[command] -help\" """

    # Currently only on MacOS
    alog = """
    "alog" command clears logcat and writes its output to console.
    This command does the same as you would write "adb logcat -c" and "adb logcat -v threadtime" on your own. """

    d = """
    "d" command prints list of all connected devices. It can also accept -m parameter to print info in copy-friendly way.
    Prints for each device:
    *	Its ID
    *	Its CPU
    *	Its GPU
    *	Its Screen resolution
    *	Its OS version
    *	Its Battery status
    *	Its UP time
    *	Its full name"""

    dc = """
    "dc" command copies all information of all currenctly connected devices
    It copies:
    *	Full name
    *	OS version
    *	CPU
    *	GPU
    *	Build release-keys"""

    ins = """
    "ins" command installs apk file into device(s)
    This script accepts 3 parameters. First one is required.
    * First parameter             - path to apk file. This apk will be installed on device.
    * Second parameter (Optional)	- Add \"-s\" if you want to auto start installed application. 
    * Third parameter (Optional)	- global variable:
    *	"-A" : installs for every connected device.
    *	"-S [device ID]" : installs application for specified device.
    * Second and Third parameters can be switched in places."""

    list = """
    Prints list of installed applications.
    Accepts global variables:
    -A : does this to all connected devices
    -S [device id] : can specify devices' ID"""

    purge = """
    Purge deletes all Unity's installed applications.
    As everywhere, 2 global variables are available:
    -A : applies script to all connected devices
    purge [deviceID] : applies script to specific device"""

    record = """
    Records video of device's screen and exports it to your PC.
    record [video_name] [target_directory]
    Target directory is optional. If target directory is not given, video will be exported to desktop."""

    screen = """
    "screen" command takes a screenshot on a specific device and store is it inside adub/screenshots folder.
    If folder does not exist - it will be created."""

    seen = """
    "seen" command prints all devices which were connected to this machine.
    It prints device's information:
    * Device's build ID
    * Supported ABI
    * Device name
    * Device model name
    * current status (ONLINE / OFFLINE)
    * (Only if status Offline) date when device was seen for the last time"""



def GetData(name):
    if name == "adub":
        print(adub2)
    elif name == "alog":
        print(alog)
    elif name == "d":
        print(d)
    elif name == "dc":
        print(dc)
    elif name == "ins":
        print(ins)
    elif name == "list":
        print(list)
    elif name == "purge":
        print(purge)
    elif name == "record":
        print(record)
    elif name == "screen":
        print(screen)
    elif name == "seen":
        print(seen)
    else:
        print("Command send via parameter was not found.")


if __name__ == '__main__':
    if len(sys.argv) > 1:
        GetData(sys.argv[1])
    else:
        print("No argumemts")
