import re
import sys

for line in sys.stdin:
        all_strings = re.split(r"\t+", line)
        if(len(all_strings) > 1):
            print ('{0}, OS:{1}, CPU:{2}, GPU:{3}, Build:{4}'.format(all_strings[7],all_strings[4],all_strings[1],all_strings[2],all_strings[8]))
