# Prerequisites:

**Installing `Ruby`:**  
1. Go to https://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-2.3.3-x64.exe  
2. Go through installation process  
3. (Make sure to select ‘Add Ruby executables to your PATH’)  

**Installing `Python3`:**  
1. https://www.python.org/ftp/python/3.6.5/python-3.6.5.exe  
2. Before pressing ‘Install Now’, make sure to select ‘Add Python 3.6 to PATH’  
3. Proceed with installation  
4. Disable MAX_PATH limitation  

**Installing `pyperclip`:**  
1. Open command line  
2. Enter `pip install pyperclip`  
- 'dc' command will now work : )  


# Setup instructions:

Install the prerequisites, then select the branch for your operating system (Windows/MacOS) and follow the instructions there!

———————————————————————————————————————

If you have any additional notes or have found something that bothers you, please report to me Tomas Zigmantavičius ([slack:@tomaszi](https://unity.slack.com/team/tomaszi#)) or Evaldas Lavrinovičius ([slack:@evaldas](https://unity.slack.com/team/evaldas)) or change it by yourself :)